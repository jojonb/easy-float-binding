﻿using Android.Views;
using Com.Lzf.Easyfloat.Interfaces;
using EasyFloatBinding.Events;
using System;

namespace EasyFloatBinding.CallBacks
{

    public class OnFloatCallbacks : Java.Lang.Object, IOnFloatCallbacks
    {
        public EventHandler<FloatCreatedEventArgs> createdResult { get; set; }
        public EventHandler dismiss { get; set; }
        public EventHandler<MotionEvent> drag { get; set; }
        public EventHandler dragEnd { get; set; }
        public EventHandler hide { get; set; }
        public EventHandler show { get; set; } 
        public EventHandler<MotionEvent> touchEvent { get; set; }

        public void CreatedResult(bool isCreated, string msg, View view)
        {
            createdResult?.Invoke(view, new FloatCreatedEventArgs()
            {
                isCreated = isCreated,
                msg = msg
            });
        }

        public void Dismiss()
        {
            dismiss?.Invoke(null, null);

        }

        public void Drag(View view, MotionEvent e)
        {
            drag?.Invoke(view, e);
        }

        public void DragEnd(View view)
        {
    
            dragEnd?.Invoke(view, null);
        }

        public void Hide(View view)
        {
            hide?.Invoke(view, null);
        }

        public void Show(View view)
        {
            show?.Invoke(view, null);
        }

        public void TouchEvent(View view, MotionEvent e)
        {
            touchEvent?.Invoke(view, e);
        }
    }
}
