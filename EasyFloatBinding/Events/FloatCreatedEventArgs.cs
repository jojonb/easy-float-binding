﻿using System;
namespace EasyFloatBinding.Events
{


    public class FloatCreatedEventArgs : EventArgs
    {
        public bool isCreated { get; set; }
        public string msg { get; set; }

    }
}